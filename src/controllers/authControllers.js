const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const {
  parsed: {SECRET_KEY, ENCODE_ROUNDS},
} = require('dotenv').config({path: './.env'});

const {User} = require('../models/userModel');

const registerUser = (req, res, next) => {
  const {username, password} = req.body;

  if (!username) {
    return res.status(400).send({
      message: 'Field \'username\' can\'t be empty',
    });
  }

  if (!password) {
    return res.status(400).send({
      message: 'Field \'password\' can\'t be empty',
    });
  }

  const user = new User({
    username: username,
    password: bcrypt.hashSync(password, bcrypt.genSaltSync(+ENCODE_ROUNDS)),
    createdDate: new Date().toISOString(),
  });

  return user
      .save()
      .then((savedUser) => {
        console.log(savedUser);

        res.status(200).send({message: 'Success'});
      })
      .catch((err) => {
        next(err);
      });
};

const loginUser = async (req, res) => {
  const {username, password} = req.body;

  if (!username) {
    return res.status(400).send({
      message: 'Provided incorrect \'username\'',
    });
  }

  if (!password) {
    return res.status(400).send({
      message: 'Provided \'password\' is incorrect',
    });
  }

  try {
    const currentUser = await User.findOne({username: username});

    // passwords is matches
    if (bcrypt.compareSync(password, currentUser.password)) {
      const token = jwt.sign(
          {
            _id: currentUser._id,
            username,
            createdDate: currentUser.createdDate,
          },
          SECRET_KEY,
          {expiresIn: '5m'},
      );

      console.log(token);

      return res.status(200).json({
        message: 'Success',
        jwt_token: token,
      });
    }

    return res.status(400).json({
      message: 'Passwords do not match',
    });
  } catch (err) {
    return res.status(400).json({message: 'Not authorized'});
  }
};

module.exports = {
  registerUser,
  loginUser,
};
