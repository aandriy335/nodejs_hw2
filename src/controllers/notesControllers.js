const {Note} = require('../models/notesModel');

const getUserNotes = async (req, res) => {
  let {offset, limit} = req.query;

  (offset = offset === undefined ? 0 : +offset),
  (limit = limit === undefined ? Infinity : +limit);

  if (+offset < 0 || +limit < 0) {
    return res
        .status(400)
        .json({message: `Query params 'offset' & 'limit' should be positive`});
  }

  try {
    const notes = await Note.find(
        {userId: req.currentUserProfile._id},
        '_id userId text completed createdDate',
    );

    const formattedNotes = notes.slice(offset, offset + limit);

    return res.status(200).json({
      offset: req.query.offset !== undefined ? +req.query.offset : undefined,
      limit: req.query.limit !== undefined ? +req.query.limit : undefined,
      count: formattedNotes.length,
      notes: formattedNotes,
    });
  } catch (err) {
    return res.status(400).json({message: err.message});
  }
};

const addNote = async (req, res) => {
  try {
    const note = new Note({
      userId: req.currentUserProfile._id,
      text: req.body.text,
      completed: false,
      createdDate: new Date().toISOString(),
    });

    const savedNote = await note.save();

    console.log(savedNote);

    return res.status(200).json({
      message: 'Success',
    });
  } catch (err) {
    return res.status(400).json({message: err.message});
  }
};

const updateNote = async (req, res) => {
  try {
    const note = req.note;

    note.text = req.body.text;

    const updatedNote = await note.save();

    console.log(updatedNote);

    return res.status(200).json({
      message: 'Success',
    });
  } catch (err) {
    return res.status(400).json({message: err.message});
  }
};

const getNote = async (req, res) => {
  return res.status(200).json({
    note: req.note,
  });
};

const toggleNoteComplete = async (req, res) => {
  try {
    const note = req.note;

    note.completed = !note.completed;

    const markedNote = await note.save();

    console.log(markedNote);

    return res.status(200).json({
      message: 'Success',
    });
  } catch (err) {
    return res.status(400).json({message: err.message});
  }
};

const deleteNote = async (req, res) => {
  try {
    const note = req.note;

    await note.delete();

    return res.status(200).json({
      message: 'Success',
    });
  } catch (err) {
    return res.status(400).json({message: err.message});
  }
};

module.exports = {
  getUserNotes,
  addNote,
  getNote,
  updateNote,
  toggleNoteComplete,
  deleteNote,
};
