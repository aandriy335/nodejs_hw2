const express = require('express');

const {
  getUserNotes,
  addNote,
  getNote,
  updateNote,
  toggleNoteComplete,
  deleteNote,
} = require('../controllers/notesControllers');

const {
  noteTextHelperMiddleware,
  noteExistsHelperMiddleware,
} = require('../middleware/noteMiddleware');

// eslint-disable-next-line new-cap
const router = express.Router();

router.route('').get(getUserNotes).post(noteTextHelperMiddleware, addNote);
router
    .route('/:id')
    .get(noteExistsHelperMiddleware, getNote)
    .put(noteTextHelperMiddleware, noteExistsHelperMiddleware, updateNote)
    .patch(noteExistsHelperMiddleware, toggleNoteComplete)
    .delete(noteExistsHelperMiddleware, deleteNote);

module.exports = {
  notesRouter: router,
};
