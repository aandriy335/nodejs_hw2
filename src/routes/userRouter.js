const express = require('express');

const {
  getUserProfile,
  deleteUserProfile,
  updateUserPassword,
} = require('../controllers/userControllers');

// eslint-disable-next-line new-cap
const router = express.Router();

router
    .get('', getUserProfile)
    .delete('', deleteUserProfile)
    .patch('', updateUserPassword);

module.exports = {
  userRouter: router,
};
